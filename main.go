package main

import (
	"github.com/zcong1993/grpc-server/acme"
	pb "github.com/zcong1993/grpc-server/services"
	"github.com/zcong1993/grpc-server/utils"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"log"
	"net"
	"os"
	"fmt"
)

var (
	port  = os.Getenv("PORT")
	host  = os.Getenv("HOST")
	email = os.Getenv("EMAIL")
)

const cacheDir = "/var/www/.cache"

type server struct{}

func (s *server) Ulid(ctx context.Context, in *pb.UlidRequest) (*pb.UlidResponse, error) {
	nums := in.Nums
	if nums > 100 {
		nums = 100
	}
	log.Printf("invoke ulid %d", nums)
	ulids := utils.GenUlids(nums)
	return &pb.UlidResponse{Ulids: ulids}, nil
}

func main() {
	if port == "" {
		port = "9393"
	}
	tls, err := acme.GetTLS(host, cacheDir, email)
	if err != nil {
		log.Fatalf("failed to get acme cert: %v", err)
	}
	creds := credentials.NewTLS(tls)
	fmt.Printf("%+v\n", creds)
	s := grpc.NewServer([]grpc.ServerOption{grpc.Creds(creds)}...)
	pb.RegisterServicesServer(s, &server{})
	lis, err := net.Listen("tcp", ":"+port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
